<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'index.twig', [
        'menuText'          => 'Меню',
        'metaTitle'         => 'Belka-z | Главная',
        'metaDescription'   => 'Главная страница',
        'metaKeywords'      => '',
    ]);
});

$app->get('/portfolio', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'portfolio.twig', [
        'menuText'          => 'Портфолио',
        'metaTitle'         => 'Belka-z | Портфолио',
        'metaDescription'   => 'Страница портфолио',
        'metaKeywords'      => '',
    ]);
});

$app->get('/contacts', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'contacts.twig', [
        'menuText'          => 'Контакты',
        'metaTitle'         => 'Belka-z | Контакты',
        'metaDescription'   => 'Страница контактов',
        'metaKeywords'      => '',
    ]);
});

$app->get('/about-us', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'about-us.twig', [
        'menuText'          => 'О компании',
        'metaTitle'         => 'Belka-z | О нас',
        'metaDescription'   => 'Страница о нас',
        'metaKeywords'      => '',
    ]);
});
?>