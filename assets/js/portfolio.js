$(function() {
    $(document).bind('mousewheel DOMMouseScroll',function (e) {
        var scrollPosition = $(window).scrollTop();
        var delta = e.originalEvent.detail || -e.originalEvent.wheelDelta;
        if (delta<0 && scrollPosition<=0) {
            $("body").css({"animation-duration" : "1.5s"}).animated("fadeOut");
            setTimeout(function () {
                location.href="/";
            }, 1000);
        }
    });
});