(function($) {

	var winWidth = window.innerWidth;

	////////ALL-PAGES
	//Плавная загрузка страницы при переходе
	if(winWidth >= 991){
		$(".animsition").animsition({
			inClass: 'fade-in',
			outClass: 'fade-out',
			inDuration: 1000,
			outDuration: 500,
			linkElement: '.animsition-link',
		});
	} else {
		var anim = $(".animsition").removeClass("animsition");
	}


	//анимация контента при загрузке страницы
	$.fn.animated = function(inEffect) {
		$(this).each(function() {
			var ths = $(this);
			ths.css("opacity", "0").addClass("animated").waypoint(function(dir) {
				if (dir === "down") {
					ths.addClass(inEffect).css("opacity", "1");
				};
			}, {
				offset: "90%"
			});

		});
	};


	if(winWidth >= 991){

		//анимация главного логотипа
		$(".main-logo, .menu-button, .portfolio-tabs").css({
			"animation-delay" : "1s",
			"animation-duration": "1s"
		}).animated("fadeIn");


		//анимация INDEX-PAGE
		var mainPage = $(".main");

		if(mainPage[0]){
			$("h2").css({"animation-delay" : "0.6s"}).animated("fadeIn");
		}

		//анимация ABOUT-US-PAGE
		var aboutUsPage = $(".about-us");

		if(aboutUsPage[0]){
			$(".about-work h2").css({"animation-duration": "1s"}).animated("fadeInLeft");
			$(".about-work .line").css({
				"animation-delay": "0.8s",
				"animation-duration": "1s"
			}).animated("fadeIn");
			$(".clints-logos").css({"animation-duration": "1s"}).animated("fadeIn");
		}

		//анимация CONTACTS-PAGE
		var contactsPage = $(".contacts");

		if(contactsPage[0]){
			$(".contacts div.address-info").css({
				"animation-delay": "1.5s",
				"animation-duration" : "1s"
			}).animated("fadeIn");
			$(".contacts p.address-info").css({
				"animation-delay": "2s",
				"animation-duration" : "1s"
			}).animated("fadeIn");
		}

		//анимация PORTFOLIO-PAGE
		var portfolioPage = $(".portfolio");

		if(portfolioPage[0]){
			$(".portfolio h2").css({
				"animation-delay": "0.9s",
				"animation-duration" : "1s"
			}).animated("fadeInDown");
			$(".work").css({"animation-duration" : "1.5s"}).animated("fadeIn");
		}

	} //end if(winWidth >= 991)



	////////HEADER
	//Изменение цвета текста и логотипа
	function checkHeaderColor(){
		var indexPage = document.getElementById("main");
		var folioPage = document.getElementById("portfolio");
		var aboutPage = document.getElementById("aboutUs");
		var contactsPage = document.getElementById("contacts");

		if(indexPage || folioPage){
			$("#open-button").css({"color": ""});
			$("#open-button .open-button").css("border-color", "");
			$(".open-button .line").css("background", "");
			$(".main-logo .logo-text").css("background-position", "");
		}
		if(aboutPage || contactsPage){
			$("#open-button").css({"color": "#fff"});
			$("#open-button .open-button").css("border-color", "#fff");
			$(".open-button .line").css("background", "#fff");
			$(".main-logo .logo-text").css("background-position", "0px -19px");
		}
	};

	checkHeaderColor();


	//показываем/прячем шапку при скроле
		navbar = $('.common-menu');
		var lastScroll = 0;

		$(window).scroll(function(event){

			var st = $(this).scrollTop();
			if (st > lastScroll && lastScroll > 300){
				window.setTimeout(function(){
					navbar[0].style.top = "-100px";
				}, 400);
			}
			else {
				window.setTimeout(function(){
					navbar[0].style.top = "";
				}, 200);
			}
			lastScroll = st;
		});


	/////////// VIDEO BACKGROUND
	//подключаем фоновое видео на главной
	$('#main').vidbg({
		'mp4': '/assets/video/test.mp4',
		// 'webm': '../libs/vidbg-master/demo/media/webm_video.webm',
		'poster': '/assets/pic/img/main-bg-1.jpg',
	},
		{ muted: true,
			loop: true,
			overlay: true,
			overlayColor: '#fff',
			overlayAlpha: '0.7',
		});


	/////////INDEX-PAGE
	//переход на портфолио при скроле страницы
	var indexPage = $(".main");
	if(indexPage[0]){

		var result = 0;
		indexPage[0].addEventListener("wheel", onWheel);

		function onWheel(e){
			 e = e || window.event;

      var delta = e.deltaY || e.detail || e.wheelDelta;
      e.preventDefault ? e.preventDefault() : (e.returnValue = false);

      result += delta;

      if(result > 300){
      	(function(){
      		var but = $(".scroll-but");
      		$(".scroll-but").trigger("click");
      	})();
      } else if (result < 0)
      	result = 0;
		};

	}


	/////////ABOUT-US
	//SVG-анимация на странице
	var ourSevise = document.getElementById("ourSevise");
	var viewHeight = document.documentElement.clientHeight;

	if(ourSevise){
		if(winWidth >= 991){
			var svgElems = ourSevise.getElementsByTagName("svg");
			var arrSvg = [];

			//проходим по svg и создаем обьекты плагина
			for(var i = 0; i < svgElems.length; i++){

				var svgId = svgElems[i].getAttribute("id");
				arrSvg[i] = new Walkway({
					selector: '#'+svgId,
					duration: '1800',
				});
			}

			//запускаем анимацию
			function calcHeight(){
				for(var i = 0; i < svgElems.length; i++){
					var coord = svgElems[i].getBoundingClientRect();
					var svgMarginTop = coord.top - viewHeight;

					if(svgMarginTop < 0){
						arrSvg[i].draw();
					}
				}
			}
			window.addEventListener("scroll", calcHeight, false);
		}


		//подезжающий футер
		if(winWidth >= 991){
			window.addEventListener("scroll", showFooter, false);
			var footer = document.getElementById("footer");
			var screenHeight = window.innerHeight;
			var contacts = $(".footer-contacts");
			contacts[0].style.top = "-500px";

			function showFooter(){
				var footRect = footer.getBoundingClientRect();

				if(footRect.top < screenHeight - 100){
					contacts[0].style.top = "0px";
				}	else {
					contacts[0].style.top = "-500px";
				}
			}
		}


	}


	/////////////CONTACTS-PAGE
	//ресайз textarea на странице контаков
	var textarea = document.getElementsByTagName("textarea");
	if(textarea.length > 0){
		autosize(document.querySelectorAll('textarea'));
	}

	//движение тени карты
	var mapWrap = document.getElementById("mapWrap");

	if(mapWrap){

			var mouseY;
			var mouseX;
			document.addEventListener("mousemove", mapShadow, false);

			function mapShadow(e){
				var timer = window.setTimeout(function(){
					mouseX = -e.pageY / 10 + 60;
					mouseY = -e.pageX / 10 + 80;

					mapWrap.style.filter = "drop-shadow("	+ mouseY/2+"px "
																								+ mouseX/2+"px "
																								+ "10px rgba(8,9,11,.6))";
				}, 150);

			}

	}

	//простая валидация формы
	var form = $(".contacts #feedBack");
	var inputs = form.find("input, textarea");
	var toSend;

	function validateInputs(){
		inputs.each(function(index, input){
			if($(input).val() != ""){
				$(input).removeClass("no-validate");
				$(input).parent().find(".error-text").remove();
				toSend = true;
			} else {
				$(input).parent().find(".error-text").remove();
				$(input).addClass("no-validate")
				.after("<p class='error-text'>Обязательно для заполнения</p>");
				toSend = false;
			}
		});
	}

	form.find("#submit").on("click", function(){

		validateInputs();

		if(toSend == false)
			return false;
	});


	//дополнение к анимации инпутов
	$(".input-effect input, .input-effect textarea").val("");

	$(".input-effect input, .input-effect textarea").focusout(function(){
		if($(this).val() != ""){
			$(this).addClass("has-content");
		}else{
			$(this).removeClass("has-content");
		}
	})


 ///////PORTFOLIO-PAGE
 var portfolio = $(".portfolio");

 if(portfolio[0]){

 //retina-img
 	var retina = window.devicePixelRatio > 1 ? true : false;
 	if(retina) {
 		$('.work img').each( function(){
 			$(this).attr('src', $(this).attr('src').replace('.jpg', '@2x.jpg'));
 		})
 	}

 }






})(jQuery);


///////////MAIN-MENU
(function() {

	var bodyEl = document.body,
		content = document.querySelector( '.viewport' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		} );
	}

	function toggleMenu() {
		if( isOpen ) {
			classie.remove( bodyEl, 'show-menu' );
			$("body, html").css("overflow", "");

		}
		else {
			classie.add( bodyEl, 'show-menu' );
			$("body, html").css("overflow", "hidden");
		}
		isOpen = !isOpen;
	}

	init();

})();
